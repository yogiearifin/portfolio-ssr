import React from "react";
import Link from "next/link";
import styles from "./Header.module.scss";
import { Button, Row, Col } from "reactstrap";

const Header = () => {
  return (
    <>
      <Col className={ styles.header }>
        <Row className={ styles.headerRow }>
          <Col className={ styles.headerName }>
            <Link href="/">
              <h1>YA</h1>
            </Link>
          </Col>
          <Col className={ styles.headerButton }>
            <Button href="/#projects">Project</Button>
            <Button
              className={ styles.resume }
              href="https://drive.google.com/file/d/1GymsxpxAVaO7hu88hDR_KI6SEKNFLYBg/view?usp=sharing"
              target="_blank"
              rel="noopener noreferrer"
            >
              Resume
            </Button>
          </Col>
        </Row>
      </Col>
    </>
  );
};

export default Header;
