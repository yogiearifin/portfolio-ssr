import React from "react";
import { Row, Col } from "reactstrap";
import styles from "./Footer.module.scss";
import { FaGithub, FaGitlab, FaMediumM } from "react-icons/fa";
import { AiOutlineMail } from "react-icons/ai";

const Footer = () => {
  const date = new Date();
  return (
    <footer>
      <Row className={ styles.footer }>
        <Col className={ styles.footerTitles }>
          <h6>{ date.getFullYear() } © Yogie Arifin</h6>
        </Col>
        <Col className={ styles.footerIcons }>
          <p>Find Yogie</p>
          <hr />
          <h1>
            <a
              href="https://github.com/yogiearifin"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaGithub />
            </a>
            <a
              href="https://gitlab.com/yogiearifin"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaGitlab />
            </a>
            <a
              href="https://medium.com/@yogiearifinp"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaMediumM />
            </a>
            <a href="mailto:yogiearifinp@gmail.com">
              <AiOutlineMail />
            </a>
          </h1>
        </Col>
      </Row>
    </footer>
  );
};

export default Footer;
