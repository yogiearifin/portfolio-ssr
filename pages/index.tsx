import Head from 'next/head';
import Intro from '../components/intro/Intro';
import ProjectList from '../components/projectList/ProjectList';

export default function Home() {
  return (
    <>
      <Head>
        <title>Yogie Arifin - Front End Developer</title>
        <meta
          name="description"
          content="Yogie Arifin's Site"
        />
        <meta name="'author" content="Yogie Arifin" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <Intro />
      <ProjectList />
    </>
  );
}
