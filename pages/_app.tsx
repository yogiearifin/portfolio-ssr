import "bootstrap/dist/css/bootstrap.min.css";
import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { Container } from 'reactstrap';
import Header from '../layout/header/Header';
import Footer from '../layout/footer/Footer';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Container className='container'>
      <Header />
      <Component { ...pageProps } />
      <Footer />
    </Container>
  );
}
