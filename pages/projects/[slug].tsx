import React from "react";
import { Row, Col, Button } from "reactstrap";
import { FaGlobe, FaGitlab, FaArrowLeft } from "react-icons/fa";
import { useRouter } from "next/router";
import Link from "next/link";
import Image from "next/image";
import { findData } from "../../helpers/helpers";
import styles from './Slug.module.scss';

const Projects = () => {
  const router = useRouter();
  const { slug } = router.query;
  const data = findData(slug, true);
  return (
    <>
      { data ? <Col className={ styles.projectsContainer }>
        <Row className={ styles.projects }>
          <Col className={ styles.projectsTitles }>
            <h1>{ data.name }</h1>
            <h6>{ data.tagline }</h6>
            <Button
              href={ `${ data.link }` }
              target="_blank"
              rel="noopener noreferrer"
              className="button"
            >
              <FaGlobe /> Live Website
            </Button>
            <Button
              href={ `${ data.repo }` }
              target="_blank"
              rel="noopener noreferrer"
              className="button"
            >
              <FaGitlab /> Repository
            </Button>
          </Col>
        </Row>
        <div className={ styles.projectsContents }>
          <Col className={ styles.projectsContentsItems }>
            <Image src={ data.img[0] } alt={ `${ data.name } introduction` } />
          </Col>
          <Col className={ styles.projectsContentText }>
            <p>
              { data.desc_head } <br /> <br />
              { data.desc }
            </p>
          </Col>
        </div>
        <Row>
          <Link href='/'>
            <Button className="button borderless">
              <FaArrowLeft /> Return to home
            </Button>
          </Link>
        </Row>
      </Col> : null }
    </>
  );
};

export default Projects;
