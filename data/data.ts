export type DataType = {
  name: string;
  slug: string;
  tagline: string;
  link: string;
  repo: string;
  img: string[];
  desc_head: string;
  desc: string;
  desc_short: string;
  video_link?: string;

};

export const data: DataType[] = [
  {
    name: "Sayembara",
    slug: "sayembara",
    tagline: "Find the Perfect Contest for You!",
    link: "https://sayembara-ga6.herokuapp.com/",
    repo: "https://gitlab.com/glints-academy-6/team-e/front-end",
    img: [
      require("../assets/images/sayembara1.webp"),
    ],
    desc_head:
      "Sayembara is an online platform for creating and joining graphic design contest.",
    desc: "In Sayembara, users are divided into two roles. The first role is provider. Providers can create contest that suits their needs. They set how much the prize money, due date, announcement date, and criteria of the contest. The second role is participant. Participants can join contests that providers create. They may submit up to three works per contest. The providers may pick them as a winner and if they do win, they can receive the prize money.",
    desc_short: "An online platform for graphic design contest.",
    video_link: "https://www.youtube.com/embed/xCECMFUDp70",
  },
  {
    name: "Hackernews",
    slug: "hackernews",
    tagline: "Hacked your newsfeed since 2020",
    link: "https://hackernews-lite.netlify.app/",
    repo: "https://github.com/yogiearifin/hackernews",
    img: [
      require("../assets/images/hn1.webp"),
    ],
    desc_head:
      "Hackernews Lite is a simple news reader that use Hackernews API.",
    desc: "The API get the news from many website and store the contents and links into their database. This site is created by Yogie to practice fetching data from API and filtering data using querry. The API is public and can be used by everyone. In this site, Yogie specifically filtered the data so that it only shows latest news, news that has many views, and job vacancy news. He also limit the news to show the maximum of 20 news.",
    desc_short: "A simple newsreader using Hackernews API.",
  },
  {
    name: "TMDB App",
    slug: "movieapp",
    tagline: "A simple TMDB app",
    link: "https://tmdb-app-one.vercel.app/",
    repo: "https://gitlab.com/yogiearifin/tmdb-app",
    img: [
      require("../assets/images/tmdb1.webp"),
    ],
    desc_head: "TMDB APP is a simple app utilizing TMDB API.",
    desc: "It allow users to browse their favorite movies, TV series, people, and many more. It also comes with many detailed info for each movie, tv series, cast member, and crew member.",
    desc_short: "A simple movie browsing app made by Yogie.",
  },
  {
    name: "Genshin Impact Characters Database App",
    slug: "jenshin-app",
    tagline: "A simple Genshin Impact characters database app",
    link: "https://jenshin-vue.vercel.app/",
    repo: "https://github.com/yogiearifin/jenshin-vue",
    img: [
      require("../assets/images/jenshin1.webp"),
    ],
    desc_head: "Genshin Impact Characters Database is a simple app made with Vue Js and Typescript utilizing Genshin Impact API made by the community.",
    desc: "It allow users to browse info about characters of Genshin Impact and mark them as their favorite.",
    desc_short: "A simple Genshin Impact characters database app made by Yogie with Vue js.",
  },
  {
    name: "Deep Rock Galactic Deep Dive Tracker",
    slug: "drg-tracker",
    tagline: "A Deep Dive tracker for hit indie video game Deep Rock Galactic",
    link: "https://drg-deep-dive-tracker.vercel.app/",
    repo: "https://github.com/yogiearifin/DRG-Deep-Dive-Tracker",
    img: [
      require("../assets/images/drg-tracker.webp"),
    ],
    desc_head: "Deep Rock Galactic Deep Dive Tracker is a simple app made with NextJs and Typescript utilizing the official Deep Dive tracker API made by Ghost Ship Games.",
    desc: "It allow users to get informations regarding the weekly Deep Dive and Deep Dive Elite in a blazingly fast webapp.",
    desc_short: "A Deep Rock Galactic Deep Dive tracker app made by Yogie with NextJs.",
  },
];