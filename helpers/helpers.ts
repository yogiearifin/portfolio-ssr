import { data } from "../data/data";

export const findData = (name: string | undefined | string[], isSlug: boolean) => {
  return data.find(item => (isSlug ? item.slug : item.name) === name);
};
