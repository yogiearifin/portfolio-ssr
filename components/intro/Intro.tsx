import React from "react";
import { Row, Col, Button } from "reactstrap";
import styles from "./Intro.module.scss";
import { FaLinkedinIn } from "react-icons/fa";

const Intro = () => {
  return (
    <React.Fragment>
      <Row className={ styles.introContainer }>
        <Col className={ styles.intro }>
          <h2>Yogie Arifin</h2>
          <h1>Front End Developer</h1>
          <Row className={ styles.introRow }>
            <Col className={ styles.introTexts }>
              <p>
                Yogie Arifin is a front-end developer that
                graduated from Glints Academy. In the academy, he learned
                many things such as HTML, CSS, Javascript, React Js, Git,
                Agile methodologies, and many more.
              </p>
              <Button
                href="https://www.linkedin.com/in/yogie-arifin"
                target="_blank"
                rel="noopener noreferrer"
                className={ styles.btnSecondary }
              >
                <FaLinkedinIn /> Connect with Yogie
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default Intro;
