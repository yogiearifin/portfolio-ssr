import React from "react";
import styles from './ProjectCard.module.scss';
import { Row, Col, Button } from "reactstrap";
import { FaArrowRight } from "react-icons/fa";
import Link from "next/link";
import { findData } from "../../helpers/helpers";

type ProjectCardType = {
  name: string;
};

const ProjectCard: React.FC<ProjectCardType> = ({ name = '' }) => {
  const dataCard = findData(name, false);
  return (
    <>
      { dataCard ? <Row className={ styles.projectRow }>
        <Col className={ styles.projectWrapper }>
          <Col>
            <h2>{ dataCard.name }</h2>
            <p>{ dataCard.desc_short }</p>
          </Col>
          <Col>
            <Link href={ `/projects/${ dataCard.slug }` }>
              <Button className={ `button ${ styles.projectButtons }` }>
                <FaArrowRight /> Learn More{ " " }
              </Button>
            </Link>
          </Col>
        </Col>
      </Row> : null }

    </>
  );
};

export default ProjectCard;
