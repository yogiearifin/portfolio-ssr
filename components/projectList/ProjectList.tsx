import React from "react";
import styles from './ProjectList.module.scss';
import { Row, Col } from "reactstrap";
import ProjectCard from "../projectCard/ProjectCard";
import { data, DataType } from "../../data/data";

const ProjectList = () => {
  return (
    <>
      <Row id="projects">
        <Col className={ styles.project }>
          <h1>Latest Project</h1>
          { data.map((item: DataType, index: number) => {
            return (
              <div key={ index }>
                <ProjectCard name={ item.name } />
              </div>
            );
          }) }
        </Col>
      </Row>
    </>
  );
};

export default ProjectList;
